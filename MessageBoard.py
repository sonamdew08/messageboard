from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import parse_qs

memory = []

form = '''<!DOCTYPE html>
  <title>Message Board</title>
  <form method="POST" action="/">
    <textarea name="message"></textarea>
    <br>
    <button type="submit">Post it!</button>
  </form>
  <pre>
{}
  </pre>
'''


class MessageHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        
        length = int(self.headers.get('Content-length', 0))
        print(length)
        
        data = self.rfile.read(length).decode()
        print(data)

        message = parse_qs(data)["message"][0]
        print(message)
        
        message = message.replace("<", "&lt;")

        memory.append(message)

        self.send_response(303)
        self.send_header('Location', '/')
        self.end_headers()

    def do_GET(self):
        
        self.send_response(200)

        self.send_header('Content-type', 'text/html; charset=utf-8')
        self.end_headers()

        msg = form.format("\n".join(memory))
        print(msg)
        self.wfile.write(msg.encode())

if __name__ == '__main__':
    server_address = ('', 8000)
    httpd = HTTPServer(server_address, MessageHandler)
    httpd.serve_forever()
